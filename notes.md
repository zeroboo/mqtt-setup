## Persistent Session

## Clean Session

Install on Kubernetes:
  <https://docs.emqx.com/en/emqx-operator/latest/deployment/on-gcp-gke.html#connecting-to-emqx-cluster-to-publish-subscribe-messages-using-mqtt-x-cli>
  
Install cert manager: <https://cert-manager.io/docs/installation/compatibility/#gke>

EMQX Kubernetes Operator:
  Link: <https://docs.emqx.com/en/emqx-operator/latest/getting-started/getting-started.html>
  
  What: is a new way to create and manage cloud-native EMQX Enterprise instances based on Kubernetes architectures.
  Does:
    Simplified Deployment EMQX:
      Declare: EMQX clusters with EMQX custom resources and deploy them quickly.
      Details: <https://github.com/emqx/emqx-operator/blob/main/docs/en_US/getting-started/getting-started.md>
    Manage EMQX Cluster:
      Automate operations and maintenance for EMQX including:
        - cluster upgrades,
        - runtime data persistence,
        - updating Kubernetes resources based on the status of EMQX, etc.
      Details: <https://github.com/emqx/emqx-operator/blob/main/docs/en_US/tasks/overview.md>

  Simplified Deployment EMQX: Declare EMQX clusters with EMQX custom resources and deploy them quickly. For more details, please check Quick Start.

  Features:
    - Quickly deploy EMQX Enterprise cluster without complicated configuration
    - Rolling update of EMQX Enterprise cluster based on changes in definition
    - Automatic scaling of EMQX Enterprise
    - Auto discover EMQX Listeners, and bind listeners port to kubernetes service
    - When updating EMQX Plugin Custom Resources, like change listener port, or update plugin configure, it will not restart Pods
    - Upgrade the EMQX Enterprise version without interrupting the service
    - Monitoring of clusters and nodes can be integrated with Prometheus
    - Official EMQX Enterprise container image is integrated
    - New stateless node: EMQX Replicant, use Deployment((EMQX Operator >= 2.0))
EMQX operator on kubernetes:
  <https://github.com/emqx/emqx-operator>
Google reference Architecture:
  Link: <https://cloud.google.com/architecture/connected-devices/mqtt-broker-architecture>
  MQTT:
    is: an OASIS standard protocol for connected device applications that provides bidirectional messaging using a publish-and-subscribe broker architecture.
    characteristics:
      lightweight to reduce the network overhead
      MQTT clients are very small to minimize the use of resources on constrained devices.
  
This document is part of a series of documents that provide information about IoT architectures on Google Cloud and about migrating from IoT Core. The other documents in this series include the following:

## Configurations

### 1. Configuration Manual

[Configuration Manual](https://www.emqx.io/docs/en/v5.1/configuration/configuration-manual.html)

### 2. Configuration files

- Configuration file path

| Installation | Path |
|---|---|
|  Installed with RPM or DEB package | /etc/emqx/emqx.conf |
|Running in docker container|/opt/emqx/etc/emqx.conf
|Extracted from portable compressed package|./etc/emqx.conf|

### 2. Configuration by Environment Variable

- Uses double underscores `"__"` as the configuration separator instead of `"."`
- Add a prefix EMQX_ to the environment variable

### 3. Configure Override Rules

Configure Override Rules

### 4. HOCON Configuration Format

HOCON is a format for human-readable data and a superset of JSON.
HOCON values can be represented as JSON-like objects or flattening

### 5. Config Cluster
