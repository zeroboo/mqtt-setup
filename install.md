
Install:

1. Install cert-manager

```console
# Install: Must have ip address

SET EXTERNAL_IP=34.136.250.162
helm upgrade --install cert-manager helm/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true --set controller.service.loadBalancerIP=%EXTERNAL_IP%

# Uninstall 
# helm uninstall cert-manager --namespace cert-manager
```

2. Install EMQX operator:

```console

helm upgrade --install emqx-operator helm/emqx-operator --namespace emqx-operator-system --create-namespace
```

3. Wait till EMQX Operator is ready:

```console
kubectl wait --for=condition=Ready pods -l "control-plane=controller-manager" -n emqx-operator-system
```

4. Deploy emqx5:
kubectl apply -f k8/emqx5.yaml

5. Setup TLS

- <https://github.com/emqx/emqx-operator/discussions/312>
- <https://cloud.google.com/architecture/connected-devices/mqtt-broker-architecture>
- <https://www.emqx.com/en/blog/emqx-server-ssl-tls-secure-connection-configuration-guide>

- <https://www.emqx.com/en/blog/two-way-tls-ssl-with-emqx-cloud#set-up-two-way-tls-ssl-in-emqx-cloud-deployment>

- <https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/>
<https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/> <https://kubernetes.github.io/ingress-nginx/examples/tls-termination/>
- <https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/> - <https://kubernetes.github.io/ingress-nginx/examples/tls-termination/>

- <https://earthly.dev/blog/mutual-tls-kubernetes-nginx-ingress-controller/>

- install nginx ingress controller <https://kubernetes.github.io/ingress-nginx/deploy/>

### Install steps nginx ingress controller

```bash
# Move to folder helm/nginx-ingress
# Install using helm from folder nginx-ingress, this command is idempotent
helm upgrade --install ingress-nginx ./ --namespace ingress-nginx --create-namespace --set controller.service.loadBalancerIP=34.143.244.87

# Check pods
kubectl get pods --namespace=ingress-nginx

# Enable cluster-admin for GKE
kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)

# Check ingress controller version
SET POD_NAMESPACE=ingress-nginx
kubectl get pods -n %POD_NAMESPACE% -l app.kubernetes.io/name=nginx-ingress --field-selector=status.phase=Running -o name > tmp.txt
set /P POD_NAME=<tmp.txt
echo kubectl exec %POD_NAME% -n %POD_NAMESPACE% -- /nginx-ingress --version

# Get controller status
kubectl get service ingress-nginx-nginx-ingress-controller --namespace=ingress-nginx
```

# Reserve static ip

- <https://serverfault.com/questions/1053305/ingress-nginx-gce-and-static-ip>

```bash
gcloud compute addresses create ingress-nginx-lb --addresses 35.240.248.39 --region asia-southeast1
```

#### 2. Test install

kubectl create ingress demo-localhost --class=nginx  --rule="demo.localdev.me/*=demo:80"
Get service IP:

```bash
 kubectl get service ingress-nginx-controller --namespace=ingress-nginx
```

### CheatSheet

```

# Get Cluster status

kubectl get emqx

# Retrieve dashboard IP

kubectl get svc emqx-dashboard -o json | jq ".status.loadBalancer.ingress[0].ip"

# Retrieve cluster IP
kubectl get svc emqx-listeners -o json | jq ".status.loadBalancer.ingress[0].ip"
```
